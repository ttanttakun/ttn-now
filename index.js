var io = require('socket.io').listen(1338);
var request = require('request');

var metadata = null;
var stats = null;

var SECRET = process.env.NOW_SECRET;
var ICECAST_HOST = process.env.ICECAST_HOST;

io.sockets.on('connection', function (socket) {
  if(metadata){
    socket.emit('metadata',metadata);
  }
  if(stats){
    socket.emit('stats', stats);
  }
  socket.on('metadata', function (data) {
    if(data.secret === SECRET){
      delete data.secret;
      metadata = data;
      socket.broadcast.emit('metadata',data);
    }
  });
});


setInterval(function(){

  request('http://' + ICECAST_HOST + ':8000/status-json.xsl', function (error, response, body) {
    if (!error && response.statusCode == 200) {
      stats = JSON.parse(body);
      io.emit('stats',stats);
    }
  });

},10000);
