FROM node:alpine

WORKDIR /var/www
COPY index.js index.js
COPY package.json package.json
RUN npm install

CMD ["npm","start"]
